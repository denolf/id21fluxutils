from setuptools import setup, find_packages

setup(
    name="id21fluxutils",
    version="0.0.1",
    url="https://gitlab.esrf.fr/denolf/id21fluxutils.git",
    author="Wout De Nolf",
    author_email="wout.de_nolf@esrf.fr",
    description="Calculate flux at ID21",
    packages=find_packages(),
    package_data={"id21fluxutils.resources": ["*/*.*"]},
    install_requires=["numpy", "scipy", "pint", "silx", "matplotlib"],
    license="MIT",
)
