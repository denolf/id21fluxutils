import numpy
import xraylib
import json
from id21fluxutils.materials import Element
from id21fluxutils.resources import resource_filename


def generate_massatt(element):
    mat = Element(element)
    filename = resource_filename(f"xraylib/{mat.symbol}_massatt.dat")

    a = 0.1
    b = 100
    inc = 1e-3
    x = numpy.linspace(a, b, int(numpy.round((b - a) / inc + 1)))
    try:
        y = mat.mass_att_coeff(x)
    except ValueError as e:
        print(e, mat)
        return
    data = numpy.vstack([x, y]).T
    numpy.savetxt(filename, data)
    print(f"Saved {filename}")


def generate_elements():
    adict = {}
    Z = 0
    while True:
        Z += 1
        try:
            symbol = xraylib.AtomicNumberToSymbol(Z)
        except ValueError:
            break
        try:
            density = xraylib.ElementDensity(Z)
        except ValueError:
            density = numpy.nan
        adict[Z] = {
            "symbol": symbol,
            "density": density,
        }
    filename = resource_filename("xraylib/elements.json")
    with open(filename, "w") as outfile:
        json.dump(adict, outfile, indent=2)
    print(f"Saved {filename}")
    return adict


if __name__ == "__main__":
    adict = generate_elements()
    for element in ["Si"]:
        generate_massatt(element)
