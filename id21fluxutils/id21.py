import numpy
from . import pndiode
from .quantity import quantity
from .resources import resource_filename


class PTB(pndiode.CalibratedPNdiode):
    def __init__(self, **kwargs):
        if not "thickness" in kwargs:
            kwargs["thickness"] = quantity(30, "um")
        tbl = numpy.loadtxt(resource_filename("idet/ptb.dat"))
        kwargs["tbl_energy"] = quantity(tbl[:, 0], "keV")
        kwargs["tbl_response"] = quantity(tbl[:, 1], "milliampere/watt")
        kwargs.setdefault("usetable", False)
        super().__init__(**kwargs)


class IDET(pndiode.CalibratedPNdiode):
    def __init__(self, npop=None, **kwargs):
        if not "thickness" in kwargs:
            kwargs["thickness"] = quantity(300, "um")
        kwargs["tbl_energy"], kwargs["tbl_response"] = self._load_response_tbl(
            npop=npop
        )
        kwargs.setdefault("fitresponse", False)
        super().__init__(**kwargs)

    @staticmethod
    def _load_response_tbl(npop=None):
        ird = numpy.loadtxt(resource_filename("idet/ird.dat"))

        # Manipulate table relative responce table
        if npop is None:
            npop = 4
        j = ird.shape[0] - npop
        energy = ird[:j, 0]  # keV
        responseratio = ird[:j, 1]

        energyadd = 8.4
        if energy[-1] < energyadd:
            responseratio = numpy.append(responseratio, 3.22)
            energy = numpy.append(energy, energyadd)

        # Tabulated response is given as a ratio with respect
        # to a reference diode (ptb)
        response = responseratio * PTB().spectral_responsivity(energy)
        return energy, response
