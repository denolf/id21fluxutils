import pint

ureg = pint.UnitRegistry()
ureg.default_format = "~e"


def quantity(x, u="dimensionless"):
    """
    :param num, array or Quantity: x
    :param str or Unit: u
    """
    try:
        return x.to(u)
    except AttributeError:
        return ureg.Quantity(x, u)
