import numpy
import silx.math.fit as fit
import matplotlib.pyplot as plt
import scipy.interpolate
from .quantity import quantity, ureg
from .materials import Material


class Oscillator:
    """Oscillator: current (A) to counts/sec (Hz)
    I(Hz) = Fmax(Hz)/FVmax(V) . Vmax(V)/Imax(A) . I(A) + F0(Hz)
      Fmax, F0, FVmax: VOF
      Vmax, Imax: Keithley
    """

    def __init__(self, F0=0, Fmax=1e6, FVmax=10, Vmax=10, Imax=1e-6, emgain=None):
        """
        :param num F0: VTOF offset
        :param num Fmax: VTOF maximal frequency
        :param num FVmax: VTOF input voltage
        :param num Vmax: Keithley output voltage
        :param num Imax: Keithley max current
        :param num emgain: Keithley gain (Imax is ignored when provided)
        """
        self.F0 = quantity(F0, "Hz")
        self.Fmax = quantity(Fmax, "Hz")
        self.FVmax = quantity(FVmax, "V")
        self.Vmax = quantity(Vmax, "V")
        self.Imax = Imax
        if emgain is not None:
            self.emgain = emgain

    def __str__(self):
        return "\n".join(
            [
                f"offset = {self.F0}",
                f"gain = {self.gain}",
                f" Fmax = {self.Fmax}",
                f" FVmax = {self.FVmax}",
                f" Vmax = {self.Vmax}",
                f" Imax = {self.Imax}",
                f" Vmax/Imax = {self.emgain}",
            ]
        )

    @property
    def gain(self):
        return (self.Fmax * self.Vmax) / (self.FVmax * self.Imax)

    @property
    def emgain(self):
        return self.Vmax / self.Imax

    @emgain.setter
    def emgain(self, value):
        self.Imax = self.Vmax / quantity(value, "V/A")

    @property
    def Imax(self):
        return self._Imax

    @Imax.setter
    def Imax(self, value):
        self._Imax = quantity(value, "A")

    def current_to_cps(self, current):
        return quantity(self.gain * quantity(current, "A") + self.F0, "Hz")

    def cps_to_current(self, cps):
        return quantity((quantity(cps, "Hz") - self.F0) / self.gain, "A")


class PNdiode:
    """PNdiode: flux (ph/s) to current(A)
    I(A) = I(ph/s).E(eV/ph).1(e).(1-T)/Ehole(eV) + D(A)
         = P(W).1(e).(1-T)/Ehole(eV) + D(A)
         = P(W).R(e/eV) + D(A)
      T: transmission of the diode
      P: radiative power
      e: elementary charge
      R: spectral responsivity
    """

    # Unit of elementary charge: 1 e = 1.6e-19 C
    # Electronvolt: 1 eV = 1.6e-19 J
    # 1 A/W = 1 C/J = 1.6e-19 C/eV = 1 e/eV

    ELCHARGE = ureg.Quantity(1, ureg.elementary_charge)

    def __init__(
        self,
        material=None,
        ehole=None,
        darkcurrent=0,
        oscillator=None,
        thickness=None,
        transmission=True,
        **oscillator_kwargs,
    ):
        if material is None:
            material = Material(Si=1)
        if ehole is not None:
            material.ehole = ehole
        self.material = material
        self.darkcurrent = quantity(darkcurrent, "A")
        if oscillator is None:
            oscillator = Oscillator(**oscillator_kwargs)
        self.oscillator = oscillator
        self.thickness = quantity(thickness, "cm")
        self._transmission = transmission

    def __str__(self):
        material = "\n ".join(str(self.material).split("\n"))
        oscillator = "\n ".join(str(self.oscillator).split("\n"))
        return "\n".join(
            [
                f"Material =\n {material}",
                f" Thickness = {self.thickness}",
                f"Dark current = {self.darkcurrent}",
                f"Oscillator =\n {oscillator}",
            ]
        )

    def flux_to_current(self, energy, flux):
        Cs = self._chargepersamplephoton(energy)
        return quantity(Cs * quantity(flux, "Hz") + self.darkcurrent, "A")

    def current_to_flux(self, energy, current):
        Cs = self._chargepersamplephoton(energy)
        return quantity((quantity(current, "A") - self.darkcurrent) / Cs, "Hz")

    def flux_to_cps(self, energy, flux):
        current = self.flux_to_current(energy, flux)
        return self.oscillator.current_to_cps(current)

    def cps_to_flux(self, energy, cps):
        current = self.oscillator.cps_to_current(cps)
        return self.current_to_flux(energy, current)

    def absorbance(self, energy, thickness=None):
        if thickness is None:
            thickness = self.thickness.to("cm").magnitude
        return self.material.mass_att_coeff(energy) * self.material.density * thickness

    def transmission(self, energy, thickness=None):
        if thickness is None:
            thickness = self.thickness.to("cm").magnitude
        return numpy.exp(-self.absorbance(energy, thickness))

    def attenuation(self, energy, thickness=None):
        if thickness is None:
            thickness = self.thickness.to("cm").magnitude
        return 1 - self.transmission(energy, thickness)

    def _spectral_responsivity_infthick(self):
        """Generated current per radiative power for an infinitely thick diode

        :returns Quantity: A/W (same as e/eV)
        """
        return (self.ELCHARGE / self.material.ehole.to("eV")).to("A/W")

    def spectral_responsivity(self, energy):
        """Generated current per radiative power

        :param num, array or Quantity energy: keV by default
        :returns Quantity: A/W (same as e/eV)
        """
        return self.attenuation(energy) * self._spectral_responsivity_infthick()

    def _fitfunc_spectral_responsivity(self, energy, thickness, ehole):
        """Function to fit measured spectral responsivity"""
        return self.attenuation(energy, thickness) / ehole

    def fit_spectral_responsivity(self, energy, response):
        """Calculate d and Ehole by fitting:

            I(A) = P(W) . 1(e) . (1-exp(-mu.rho.d))/Ehole(eV) + D(A)
            response(A/W) = (I(A)-D(A))/P(W) = 1(e).(1-exp(-mu.rho.d))/Ehole(eV)

        :param num, array or Quantity energy: keV by default
        :param num, array or Quantity response: A/W by default
        """
        ehole = self.material.ehole.to("eV").magnitude
        thickness = self.thickness.to("cm").magnitude
        response = quantity(response, "A/W").magnitude
        energy = quantity(energy, "keV").magnitude
        (thickness, ehole), cov_matrix = fit.leastsq(
            self._fitfunc_spectral_responsivity, energy, response, [thickness, ehole]
        )
        self.thickness = quantity(thickness, "cm")
        self.material.ehole = quantity(ehole, "eV")

    def plot_spectral_responsivity(self, energy):
        plt.plot(
            energy,
            self.spectral_responsivity(energy).to("A/W"),
            label=self.__class__.__name__,
        )
        ax = plt.gca()
        ax.set_xlabel("Energy (keV)")
        ax.set_ylabel("Spectral responsivity (A/W)")
        ax.get_yaxis().get_major_formatter().set_useOffset(False)
        plt.legend()

    def plot_response(self, energy, flux, input="flux"):
        """
        :param num, array or Quantity energy: keV by default
        :param num, array or Quantity flux: Hz by default
        :param str input: "current" or "flux"
        """
        if input == "current":
            response = self.flux_to_current(energy, flux)
        elif input == "flux":
            response = self.flux_to_cps(energy, flux)
        else:
            raise ValueError("Unknown input")
        unit = response.unit
        response = response.magnitude
        for i, (x, y) in enumerate(zip(energy, response)):
            plt.plot(x, y, label="Source{}".format(i))
        # plt.axhline(y=self.oscillator.Fmax.to("Hz").magnitude,label="max")
        ax = plt.gca()
        ax.set_xlabel("Energy (keV)")
        ax.set_ylabel("Response ({})".format(unit))
        ax.get_yaxis().get_major_formatter().set_useOffset(False)
        plt.title(
            "{} @ {:.02e} ph/s, {:~.0e}".format(
                self.__class__.__name__, flux, self.oscillator.gain
            )
        )
        plt.legend()

    def _chargeperdiodephoton(self, energy):
        """Charge generated per photon hitting the diode:
        spectral responsivity multiplied by the energy

        :param num, array or Quantity energy: keV by default
        :param Quantity: C/ph
        """
        return self.spectral_responsivity(energy) * quantity(energy, "keV")

    def _chargepersamplephoton(self, energy):
        """Charge generated per photon hitting the sample

        :param num, array or Quantity energy: keV by default
        :param Quantity: C/ph
        """
        Cs = self._chargeperdiodephoton(energy)
        if not self._transmission:
            Cs *= self.transmission(energy)
        return Cs


class CalibratedPNdiode(PNdiode):
    """A pn-diode with a known spectral responsivity"""

    def __init__(
        self,
        tbl_energy=None,
        tbl_response=None,
        usetable=True,
        fitresponse=True,
        **kwargs,
    ):
        """
        :param num, array or Quantity tbl_energy(array): corresponding energies for response
        :param num, array or Quantity tbl_response(array): spectral responsivity (A/W)
        :param bool usetable: use the fitted spectral responsivity or
                              interpolate the energy/response table
        :param bool fitresponse: modify thickness and Ehole to response
        """
        super(CalibratedPNdiode, self).__init__(**kwargs)
        self.usetable = usetable
        self.tbl_energy = quantity(tbl_energy, "keV")
        self.tbl_response = quantity(tbl_response, "A/W")
        self.fitresponse = fitresponse
        self._init_response()

    def _init_response(self):
        if self.fitresponse:
            self.fit_spectral_responsivity(self.tbl_energy, self.tbl_response)
        self.finterpol = scipy.interpolate.interp1d(
            self.tbl_energy.magnitude,
            self.tbl_response.magnitude,
            bounds_error=False,
            fill_value=numpy.nan,
        )

    def spectral_responsivity(self, energy):
        """
        Args:
            energy(num or array): keV

        Returns:
            num or array: A/W
        """
        energy = quantity(energy, "keV")
        if self.usetable:
            r = self.finterpol(energy.magnitude)
            ind = numpy.isnan(r)
            try:
                if any(ind):
                    r[ind] = super().spectral_responsivity(energy[ind])
            except TypeError:
                if ind:
                    r = super().spectral_responsivity(energy)
        else:
            r = super().spectral_responsivity(energy)
        return quantity(r, "A/W")

    def plot_spectral_responsivity(self, energy):
        mi, ma = energy.min(), energy.max()
        tbl_energy = self.tbl_energy.magnitude
        mask = (tbl_energy >= mi) & (tbl_energy <= ma)
        if mask.any():
            plt.plot(
                tbl_energy[mask],
                self.tbl_response.magnitude[mask],
                "o",
                label=f"{self.__class__.__name__} (Tabulated)",
            )
        super().plot_spectral_responsivity(energy)
