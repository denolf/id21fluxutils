import os
import numpy
import scipy.interpolate
from .quantity import quantity
from .resources import resource_filename

try:
    import xraylib
except ImportError:
    xraylib = None
    import json

    with open(resource_filename("xraylib/elements.json"), mode="r") as json_file:
        element_info = json.load(json_file)
        element_symbol_to_num = {v["symbol"]: Z for Z, v in element_info.items()}


ehole = {"Si": quantity(3.68, "eV")}


def get_from_resource(method, Z, energy):
    energy = quantity(energy, "keV").magnitude
    func = element_info[Z].get(method)
    if func is None:
        symbol = element_info[Z]["symbol"]
        filename = resource_filename(f"xraylib/{symbol}_{method}.dat")
        tbl_x, tbl_y = numpy.loadtxt(filename).T
        func = scipy.interpolate.interp1d(
            tbl_x,
            tbl_y,
            bounds_error=False,
            fill_value=numpy.nan,
        )
        element_info[Z][method] = func
    return func(energy)


def get_from_xraylib(method, Z, energy):
    """
    :param str method:
    :param int Z:
    :param num, array or Quantity energy: keV by default
    :returns num or array:
    """
    func = getattr(xraylib, method)
    energy = quantity(energy, "keV").magnitude
    try:
        return numpy.array([func(Z, e) for e in energy])
    except TypeError:
        return func(Z, energy)


class Element:
    def __init__(self, element):
        if isinstance(element, str):
            self.symbol = element
            if xraylib is None:
                self.Z = element_symbol_to_num[self.symbol]
            else:
                self.Z = xraylib.SymbolToAtomicNumber(self.symbol)
        else:
            self.Z = int(element)
            if xraylib is None:
                self.symbol = element_info[self.Z]["symbol"]
            else:
                self.symbol = xraylib.AtomicNumberToSymbol(self.Z)

    def __repr__(self):
        return self.symbol

    @property
    def density(self):
        if xraylib is None:
            return element_info[self.Z]["density"]
        else:
            return xraylib.ElementDensity(self.Z)

    @property
    def ehole(self):
        return ehole.get(self.symbol, numpy.nan)

    def mass_att_coeff(self, energy):
        if xraylib is None:
            return get_from_resource("massatt", self.Z, energy)
        else:
            return get_from_xraylib("CS_Total_Kissel", self.Z, energy)


class Material:
    def __init__(self, density=None, ehole=None, **kwargs):
        self._wfrac = {Element(e): w for e, w in kwargs.items()}
        e = self._element
        if e is not None:
            if density is None:
                density = e.density
            if ehole is None:
                ehole = e.ehole
        self.density = density
        self.ehole = ehole

    def __str__(self):
        return f"wt% = {self._wfrac}\nIonization energy = {self.ehole}"

    def mass_att_coeff(self, energy):
        return sum(w * e.mass_att_coeff(energy) for e, w in self._wfrac.items())

    @property
    def _element(self):
        if len(self._wfrac) == 1:
            return next(iter(self._wfrac.keys()))

    @property
    def ehole(self):
        return self._ehole

    @ehole.setter
    def ehole(self, value):
        self._ehole = quantity(value, "eV")
