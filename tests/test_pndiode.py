import pytest
import numpy
import scipy.interpolate
from id21fluxutils import id21
from id21fluxutils.quantity import quantity
from id21fluxutils import pndiode
from id21fluxutils.resources import resource_filename


def spec_idet_cpstoflux(ph_E, ph_I, ph_gain):
    """id21photons.mac

    :param ph_E: keV
    :param ph_I: idet counts
    :param ph_gain: SXM V/A
    """
    elementary_charge = 1.6e-19
    PH_DIST = 0
    ph_coeff = 0
    ph_I = ph_I * 10.0 ** (-5 - ph_gain)
    ptb = numpy.loadtxt(resource_filename("idet/ptb.dat"))
    fptb = scipy.interpolate.interp1d(ptb[:, 0], ptb[:, 1])
    ird = numpy.loadtxt(resource_filename("idet/ird.dat"))
    fird = scipy.interpolate.interp1d(ird[:, 0], ird[:, 1])
    ph_PTB = fptb(ph_E)
    ph_factor = fird(ph_E)
    ph_calib = ph_factor * ph_PTB
    return ph_I / (ph_E * elementary_charge * numpy.exp(-ph_coeff * PH_DIST) * ph_calib)


def test_pndiode():
    diode1 = pndiode.PNdiode(thickness=quantity(300, "um"))
    diode2 = id21.IDET()

    def assert_pndiode():
        roundtrip = diode.current_to_flux(energy, diode.flux_to_current(energy, flux))
        numpy.testing.assert_allclose(roundtrip, flux)
        roundtrip = diode.cps_to_flux(energy, diode.flux_to_cps(energy, flux))
        numpy.testing.assert_allclose(roundtrip, flux)

    for diode in [diode1, diode2]:
        energy = 5
        flux = 1e7
        assert_pndiode()
        energy = 5
        flux = numpy.linspace(1e5, 1e9, 100)
        assert_pndiode()
        energy = numpy.linspace(4, 10, 2)
        flux = numpy.linspace(1e5, 1e9, 2)
        assert_pndiode()


def test_compare_spectrocrunch_ptb():
    try:
        from spectrocrunch.detectors.diode import factory
    except ImportError:
        pytest.skip("spectrocrunch not installed")
    energy = 5
    emgain = 1e5

    refdet = factory("ptb")
    diode = id21.PTB(ehole=refdet.ehole.to("eV").magnitude)
    refdet.gain = emgain
    diode.oscillator.emgain = emgain

    print("Spectrocrunch")
    print(refdet)
    print("\nThis:")
    print(diode)

    a = refdet.thickness
    b = diode.thickness.to("cm").magnitude
    numpy.testing.assert_allclose(a, b)

    a = refdet.attenuation(energy)
    b = diode.attenuation(energy)
    numpy.testing.assert_allclose(a, b)

    a = refdet._spectral_responsivity_infthick().to("A/W").magnitude
    b = diode._spectral_responsivity_infthick().to("A/W").magnitude
    numpy.testing.assert_allclose(a, b)

    a = refdet.mresponse
    b = diode.tbl_response.to("mA/W").magnitude
    numpy.testing.assert_allclose(a, b)

    a = refdet.spectral_responsivity(energy).to("A/W").magnitude
    b = diode.spectral_responsivity(energy).to("A/W").magnitude
    numpy.testing.assert_allclose(a, b)


def test_compare_idet_spec():
    diode = id21.IDET()
    diode.oscillator.emgain = 1e6
    a = diode.cps_to_flux(5, 1000).to("Hz").magnitude
    gain = diode.oscillator.emgain.to("V/A").magnitude
    b = spec_idet_cpstoflux(5, 1000, numpy.log10(gain))
    numpy.testing.assert_allclose(a, b, rtol=0.02)


def test_compare_spectrocrunch_idet():
    try:
        from spectrocrunch.detectors.diode import factory
    except ImportError:
        pytest.skip("spectrocrunch not installed")
    energy = 5
    flux = 1e10
    emgain = 1e5

    refdet = factory("idet")
    diode = id21.IDET(ehole=refdet.ehole.to("eV").magnitude)
    refdet.gain = emgain
    diode.oscillator.emgain = emgain

    print("Spectrocrunch")
    print(refdet)
    print("\nThis:")
    print(diode)

    a = refdet.thickness
    b = diode.thickness.to("cm").magnitude
    numpy.testing.assert_allclose(a, b)

    a = refdet.attenuation(energy)
    b = diode.attenuation(energy)
    numpy.testing.assert_allclose(a, b)

    a = refdet._spectral_responsivity_infthick().to("A/W").magnitude
    b = diode._spectral_responsivity_infthick().to("A/W").magnitude
    numpy.testing.assert_allclose(a, b)

    a = refdet.mresponse
    b = diode.tbl_response
    numpy.testing.assert_allclose(a, b)

    a = refdet.spectral_responsivity(energy).to("A/W").magnitude
    b = diode.spectral_responsivity(energy).to("A/W").magnitude
    numpy.testing.assert_allclose(a, b)

    a = refdet._chargeperdiodephoton(energy).to("e").magnitude
    b = diode._chargeperdiodephoton(energy).to("e").magnitude
    numpy.testing.assert_allclose(a, b)

    a = refdet._chargepersamplephoton(energy).to("e").magnitude
    b = diode._chargepersamplephoton(energy).to("e").magnitude
    numpy.testing.assert_allclose(a, b)

    a = refdet.fluxtocps(energy, flux).to("Hz").magnitude
    b = diode.flux_to_cps(energy, flux).to("Hz").magnitude
    numpy.testing.assert_allclose(a, b)
