# ID21 Flux Utilities

Calculate flux from diode response

## Dependencies

Required: numpy, scipy, pint, silx, matplotlib

Xraylib is required when dealing with non-Si diodes.

## Usage

General Si PN-diode

```python
from id21fluxutils.quantity import quantity
from id21fluxutils import pndiode
diode = pndiode.PNdiode(thickness=quantity(300, "um"))
diode.oscillator.emgain = 1e8
flux = diode.cps_to_flux(energy, response)
```

Calibrated diode (transmission diode at ID21)

```python
from id21fluxutils import id21
diode = id21.IDET()
diode.oscillator.emgain = 1e8
flux = diode.cps_to_flux(energy, response)
```

Plot spectral responsivity as a function of energy

```python
import matplotlib.pyplot as plt
diode.plot_spectral_responsivity(numpy.linspace(1, 15, 100))
plt.show()
```
